package com.example.demo.service;

import com.example.demo.domain.user.User;
import com.example.demo.domain.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class TestService {

    private final UserRepository userRepository;

    @Transactional
    public void saveUserName(String username){
        userRepository.mSave(username);
    }

    @Transactional
    public User findUserName(String username){
        return userRepository.findByUsername(username);
    }

}
