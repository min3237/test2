package com.example.demo.web;

import com.example.demo.domain.user.User;
import com.example.demo.domain.user.UserRepository;
import com.example.demo.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class ViewTestController {

    private final TestService testService;

    @GetMapping("/hello1")
    public String hello(Model model) {

        testService.saveUserName("홍길동");
        User user = testService.findUserName("홍길동");

        model.addAttribute("user",user);

        return "hello";
    }
}
